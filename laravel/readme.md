Run project: `docker-composer up -d`

Generating fake users: 
```
$ docker-compose exec web bash
$ php artisan tinker
factory('App\User', 50)->create();
```

Query the project using a gui: `docker run -d -p3000:3000 langa/graphiql.io` 
Now you have access to a gui in `http://<DOCKER_IP>:3000/`

In the gui tou must modify the URL to explore to `http://localhost/graphql`

Example:
```
query FetchUsers {
    users {
        id
        email
    }
}

mutation users {
    updateUserPassword(id: "1", password: "newpassword") {
        id
        email
    }
}

mutation users {
    updateUserEmail(id: "1", email: "test@host.com") {
        id
        email
    }
}
```