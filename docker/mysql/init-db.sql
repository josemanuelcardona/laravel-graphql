CREATE DATABASE IF NOT EXISTS `api` CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE DATABASE IF NOT EXISTS `test` CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE `test`;

CREATE DATABASE IF NOT EXISTS softonic2
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

USE softonic2;

CREATE TABLE IF NOT EXISTS`file` (
  `id_file` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `check_download` enum('check','no_check','direct') NOT NULL DEFAULT 'check',
  `download_option` tinyint(3) unsigned DEFAULT '7',
  `ts_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=69728043 DEFAULT CHARSET=latin1;

INSERT INTO `file` (`id_file`, `check_download`, `download_option`) VALUES (29287, 'direct', 5);

CREATE TABLE IF NOT EXISTS `migration_programs` (
  `id_program` char(36) NOT NULL COMMENT 'Program UUID for Catalog DB',
  `id_file` int(11) unsigned NOT NULL COMMENT 'File ID for Softonic DB',
  PRIMARY KEY (`id_program`),
  UNIQUE KEY `id_file` (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Migration programs for catalog_api_v2';

INSERT INTO `migration_programs` (`id_program`, `id_file`) VALUES ('c1a50726-96bf-11e6-8b2b-00163ec9f5fa', 29287);