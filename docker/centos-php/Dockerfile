FROM centos:7

MAINTAINER Daniel Ibáñez Fernández <daniel.ibanez@softonic.com>

LABEL Vendor="CentOS" \
      License=GPLv2 \
      Version=2.4.6-40

COPY ./docker/centos-php/config/php.ini /etc/
ADD ./docker/centos-php/auth.json /usr/share/httpd/.composer/auth.json

RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install httpd epel-release && \
    curl -LO https://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-14.ius.centos7.noarch.rpm && \
    rpm -Uvh ius-release*.rpm && \
    yum -y --setopt=tsflags=nodocs install php70u-php70u-pdo php70u-mysqlnd php70u-common php70u-cli php70u-opcache mod_php70u php70u-json php70u-mbstring php70u-bcmath php70u-xml php70u-pdo && \
    yum clean all

# Xdebug only dev
RUN yum -y --setopt=tsflags=nodocs install php70u-pecl-xdebug \
    && echo "xdebug.remote_enable=on" >> /etc/php.d/15-xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /etc/php.d/15-xdebug.ini

COPY ./docker/centos-php/config/httpd.conf /etc/httpd/conf/httpd.conf
COPY ./docker/centos-php/config/vhostphp.conf /etc/httpd/conf.d/vhosts.d/vhostphp.conf

WORKDIR /var/www/sftworkspace/laravel
COPY ./laravel /var/www/sftworkspace/laravel

# Set permissions to apache to lend php to write files in working directory & install composer
RUN chown apache: . -R \
    && php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');"

EXPOSE 80

# Simple startup script to avoid some issues observed with container restart
ADD ./docker/centos-php/run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

# Change current user to apache to install composer libraries with apache permissions.
USER apache

RUN php composer.phar install \
    && rm composer.phar

USER root

CMD ["/run-httpd.sh"]